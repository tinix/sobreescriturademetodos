package ejemplosobreescritura;

/**
 *
 * @author tinix
 */
public class Gerente extends Empleado {
    
    private String departamento;
    
    public Gerente(String nombre, double sueldo, String departamento){
        super(nombre, sueldo); 
        this.departamento = departamento;
    }
    
    public String obtenerDetalles(){
        return "Nombre: " + nombre + ", sueldo :" + sueldo + ","
                + " departamento: " + getDepartamento();
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
   
    
    
 
}
