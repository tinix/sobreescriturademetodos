
package ejemplosobreescritura;

/**
 *
 * @author tinix
 */
public class EjemploSobreescritura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Empleado empleado = new Empleado("Juan", 1000);
        System.out.println(empleado.obtenerDetalles());
        
        Gerente gerente = new Gerente("Carla", 2000 , "finanzas");
        System.out.println(gerente.obtenerDetalles()); 
    }
}
